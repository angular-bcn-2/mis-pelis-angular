import { Component, OnInit } from '@angular/core';

import { PeliculasService } from '../peliculas.service';

interface Pelicula {
  id: number;
  titulo: string;
  director: string;
  anyo: number;
  cartel: string;
  vista: boolean;
  valoracion: number;
}

@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['formulario.component.css']
})
export class FormularioComponent implements OnInit {

  public nuevaPelicula: Pelicula = {
    id: 0,
    titulo: "",
    director: "",
    anyo: null,
    cartel: "",
    vista: false,
    valoracion: 0
  };

  constructor(private peliculasService: PeliculasService) { }

  public crearPelicula(formulario): void {
    this.peliculasService.crearPelicula(this.nuevaPelicula);
    formulario.reset();
  }

  ngOnInit() {
  }

}
