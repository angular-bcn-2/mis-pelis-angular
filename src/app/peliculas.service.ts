import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { ConfigService } from './config.service';

interface Pelicula {
  id: number;
  titulo: string;
  director: string;
  anyo: number;
  cartel: string;
  vista: boolean;
  valoracion: number;
}

@Injectable()
export class PeliculasService {

  public peliculas: Array<Pelicula> = [];

  constructor(
    private http: HttpClient,
    private configService: ConfigService
  ) {
    this.http.get<Pelicula[]>(this.configService.apiUrl).subscribe(
        peliculas => {
          peliculas.forEach(pelicula => {
            this.peliculas.push(pelicula)
          })
        }
    )
  }

  public getPeliculasPendientes(): Pelicula[] {
    return this.peliculas.filter( pelicula => !pelicula.vista )
  }  

  public getPeliculasVistas(): Pelicula[] {
    return this.peliculas.filter( pelicula => pelicula.vista )
  }  

  public marcarVista(pelicula: Pelicula, puntos: number): void {
    this.http.patch(
      this.configService.apiUrl + pelicula.id, 
      { vista: true, valoracion: puntos }
    ).subscribe( respuesta => {
      pelicula.vista = true;
      pelicula.valoracion = puntos;
    });
  }

  public marcarNoVista(pelicula: Pelicula): void {
    this.http.patch(
      this.configService.apiUrl + pelicula.id, 
      { vista: false, valoracion: 0 }
    ).subscribe( respuesta => {
      pelicula.vista = false;
      pelicula.valoracion = 0;
    })
  }

  public crearPelicula(pelicula: Pelicula): void {    
    this.http.post<Pelicula>(this.configService.apiUrl, pelicula).subscribe(
      pelicula => {
        this.peliculas.push(pelicula)
      }
    );
  }

  public eliminarPelicula(pelicula: Pelicula): void {
    this.http.delete(this.configService.apiUrl + pelicula.id).subscribe(
      respuesta => {
        let posPelicula: number = this.peliculas.findIndex( peli => peli.id == pelicula.id );
        this.peliculas.splice(posPelicula, 1);
        //this.peliculas = this.peliculas.filter( peli => peli.id != pelicula.id )
      }
    )
  }

}
