import { Component, OnInit } from '@angular/core';

import { PeliculasService } from '../peliculas.service';

interface Pelicula {
  id: number;
  titulo: string;
  director: string;
  anyo: number;
  cartel: string;
  vista: boolean;
  valoracion: number;
}

@Component({
  selector: 'app-listados',
  templateUrl: './listados.component.html',
  styleUrls: ['./listados.component.css']
})
export class ListadosComponent implements OnInit {

  public avisoVisible: boolean = false;
  public avisoTitulo: string = "";
  
  constructor(private peliculasService: PeliculasService) { }

  get totalPeliculas(): number {
    return this.peliculasService.peliculas.length;
  }

  public mostrarAviso(titulo: string): void {
    this.avisoTitulo = titulo;
    this.avisoVisible = true;
    setTimeout(() => {
      this.avisoVisible = false;
      this.avisoTitulo = "";
    }, 3000);
  }

  ngOnInit() {
  }

}
