import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { CabeceraComponent } from './cabecera/cabecera.component';
import { FormularioComponent } from './formulario/formulario.component';
import { TituloComponent } from './titulo/titulo.component';
import { ListadosComponent } from './listados/listados.component';
import { ListadoComponent } from './listado/listado.component';

import { PeliculasService } from './peliculas.service';
import { ConfigService } from './config.service';

@NgModule({
  declarations: [
    AppComponent,
    CabeceraComponent,
    FormularioComponent,
    TituloComponent,
    ListadosComponent,
    ListadoComponent
  ],
  imports: [
    BrowserModule, HttpClientModule, FormsModule
  ],
  providers: [
    PeliculasService,
    ConfigService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
