import { Component, Input, EventEmitter, Output, OnInit } from '@angular/core';

import { PeliculasService } from '../peliculas.service';

interface Pelicula {
  id: number;
  titulo: string;
  director: string;
  anyo: number;
  cartel: string;
  vista: boolean;
  valoracion: number;
}

@Component({
  selector: 'app-listado',
  templateUrl: './listado.component.html',
  styles: []
})
export class ListadoComponent implements OnInit {

  @Input()
  tipo: string;

  @Output()
  marcarPelicula: EventEmitter<string> = new EventEmitter();

  claseLista: string;

  constructor(private peliculasService: PeliculasService) { }  

  get totalPeliculas(): number {
    return this.peliculasPorTipo(this.tipo).length;
  }

  public peliculasPorTipo(tipo: string): Array<Pelicula> {    
    if (tipo == 'vistas') {
      return this.peliculasService.getPeliculasVistas();
    } else if (tipo == 'pendientes') {
      return this.peliculasService.getPeliculasPendientes();
    }
    
  }

  get peliculasVistas(): Array<Pelicula> {
    return this.peliculasService.getPeliculasVistas();
  }

  puntosString(pelicula: Pelicula): string | number {
    return this.tipo == 'pendientes' ? '' : pelicula.valoracion;
  }

  tituloEstrella(pelicula: Pelicula, i: number): string | number {
    return this.tipo == 'pendientes' ? `${i}/5` : pelicula.valoracion;
  }

  descripcion(): string {
    return this.tipo == 'vistas' ? 'Vistas' : 'Pendientes de ver';
  }

  public claseEstrella(pelicula: Pelicula, i: number): string {
    if (this.tipo == 'pendientes') {
      return 'glyphicon-star-empty';
    } else {
      return i <= pelicula.valoracion ? 'glyphicon-star' : 'glyphicon-star-empty';
    }    
  }
  
  public marcarVista(pelicula: Pelicula, puntos: number): void {
    this.peliculasService.marcarVista(pelicula, puntos);
    this.marcarPelicula.emit(pelicula.titulo);
  }

  public marcarNoVista(evento: Event, pelicula: Pelicula): void {
    evento.preventDefault();
    this.peliculasService.marcarNoVista(pelicula);
  }

  public eliminarPelicula(evento: Event, pelicula: Pelicula) {
    evento.preventDefault();
    this.peliculasService.eliminarPelicula(pelicula);
  }

  ngOnInit() {
    this.claseLista = 'lista-' + this.tipo;
  }

}
